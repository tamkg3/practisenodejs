var express = require('express');
//import express from 'express';
var app = express();
var models = require('./models');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));




// set morgan to log info about our requests for development use.


// initialize body-parser to parse incoming parameters requests to req.body


// initialize cookie-parser to allow us access the cookies stored in the browser. 
// initialize express-session to allow us track the logged-in user across sessions.
//var bookRouter = require('./routes/books')
var hbs = require('express-handlebars');
//define engine name hbs, use hbs to create handlebars
app.get('/favicon.ico', (req, res) => {
    console.log('favicom.ico sent');
    res.sendFile(__dirname + '/public/favicon.ico');
})
app.use(express.static(__dirname + '/public'));
app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'layout',
    layoutsDir: __dirname + '/views/layouts',
    partialsDir: __dirname + '/views/partials'
}));

app.set('view engine', 'hbs');
app.get('/sync', function(req, res) {
    models.sequelize.sync().then(
        () => {
            res.send('db synchronized');
        }
    );
});
app.get('', (req, res) => {
    res.render('index');
});



var userRouter = require('./routes/users');
app.use('/users', userRouter);





// npm install bcrypt 

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.

app.listen(process.env.PORT || 8081, function() {
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});