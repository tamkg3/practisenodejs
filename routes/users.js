var express = require('express');
var app = express.Router();

var models = require('../models');


/*
 *add an expense
 *param.id: userid
 *body: models.expense
 */



// console.log(`${req.body.password}`);

app.post('/login', function(req, res) {
    models.User.findOne({ where: { username: req.body.username } }).then(function(user) {
        if (!user) { res.redirect('/'); } else {
            bcrypt.compare(req.body.password, user.password, function(err, result) {
                if (result == true) {
                    console.log(`login success + ${user.id}`)
                    res.redirect(`/users/${user.id}`);
                } else {
                    res.send('Incorrect password');
                    res.redirect('/');
                }
            });
        }
    });
});
app.get('/:id/form/:expenseId', (req, res) => {
    console.log('form edit an expense');

    models.Expense.findByPk(req.params.expenseId).then((expense) => {
        var context = { expenseContainer: { expense } };
        res.render('form', context);
    }).catch(error => res.json(error));

});
app.post('/:id', (req, res) => {
    //body=expenseObject
    console.log('add an expense');
    models.Expense
        .create({
            moneySpent: req.body.moneySpent,
            describe: req.body.describe,
            // UserId: req.params.id
        })
        .then(() => {

            // req.params.expense = expense;param
            res.redirect(`/users/${req.params.id}`);
        })
        //.then(expense => res.json(expense))
        .catch(error => res.json(error))
});
/*

app.get('/:id', (req, res) => {
    console.log('get all expense');
    models.Expense
        .findAll({
            where: { UserId: req.params.id }
        })
        .then(expenses => res.json(expenses))
        .catch(error => res.json(error));
});

*/
app.get('/:id', (req, res) => {

    console.log(`get all expense per month ${req.query.month}`);
    const size = parseInt(req.query.size) || 5;
    const page = parseInt(req.query.page) || 1;
    //const monthParam = req.query.month;
    var result;
    //lấy dữ liệu lên từ database,
    if (req.query.month == null) {
        models.Expense
            .findAll({
                //limit: size,
                //offset: (page - 1) * size,
                // where: { UserId: req.params.id }
            })
            .then(expenses => {
                console.log(`list contains ${expenses.length}`);
                // if (expenses.length == 0) {
                //     var context = { id: req.params.id };
                //     res.render('noExpense', context);
                // } else {
                //expenses là dữ liệu lấy được
                result = expenses;
                //tính toán xử lí
                var sum = 0;
                result.forEach(element => {
                    console.log(element.moneySpent);
                    if (element.moneySpent != null) {
                        sum = sum + parseInt(element.moneySpent);
                    }
                });

                console.log(`sum: ${sum}`) //log ra để xem coi có đúng ko thôi

                //gom data lại trong biến context, để dưới dạng  -- key:value
                var context = {
                    expenses: result,
                    total: sum
                };
                //res.json(result);
                res.render('index', context); //render ra cái body có tên là index.hbs,
                // data sẽ gồm cái đám nằm trong context
                //giờ qua cái index.hbs để xem cách lấy data lên


            })
            .catch(error => res.json(error));
    } else {
        models.Expense
            .findAll({
                //limit: size,
                //offset: (page - 1) * size,
                where: {
                    // UserId: req.params.id,
                    //createdAt: monthParam
                    //psequelize.where(sequelize.fn("month",sequelize.col("updatedAt")),monthParam)
                }
            })
            .then(expenses => {

                result = expenses

                    .filter(
                    expense => {
                        return expense.createdAt.getMonth() + 1 == req.query.month
                    }
                ).map((item, index) => {
                    item.createdAt = item.createdAt.toString().slice(0, 15);
                    console.log(item);
                    return item;
                    //console.log(item.createdAt.toString().slice(0, 15));
                });
                var sum = 0;
                // console.log(result);
                result.forEach(element => {
                    console.log(element.moneySpent);
                    sum = sum + parseInt(element.moneySpent);
                });

                console.log(`sum: ${sum}`)
                var context = {
                    expenses: result, // này á nó lưu dưới dạng json sẽ là
                    /* 
                    {
                        "expenses": result,
                        "total": sum,
                        "userId": id
                    }
                    */
                    total: sum,
                    userId: req.params.id
                };
                //res.json(result);
                res.render('index', context);
                //res.json([result, sum]);

            })
            .catch(error => res.json(error));
    }

});
app.post('/:UserId/expenses/:id', (req, res) => {
    const id = req.params.id;
    console.log(`edit expense ${req.params.id} of user ${req.params.UserId}`);
    models.Expense
        .update({
            moneySpent: req.body.moneySpent,
            describe: req.body.describe
        }, {
            where: { id: req.params.id }
        })
        .then(() => {
            //res.redirect('/' + id)
            res.redirect(`/users/${req.params.UserId}`);
        })
        .catch(error => res.json(error));
});
app.get('/:userId/expenses/:expenseId', (req, res) => {
    console.log(`${req.params.userId} delete expense ${req.params.expenseId}`);
    models.Expense
        .destroy({
            where: { id: req.params.expenseId }
        })
        .then(() => {
            //res.redirect('/' + id)
            res.redirect(`/users/${req.params.userId}`);
        })
        .catch(error => res.json(error));
});

module.exports = app;